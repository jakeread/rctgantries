``` OLD DOC: depricated hardware ```

Scratch / Landing page for the roller-bearings and 2d-sheet-stock axis system.

With RCT, we break axis into individual kinematic elements - and roll those elements into machine designs. 

![gantries](images/one-two-three.gif)

A reasonable system for beginners, and easy to manufacture (with another CNC mill). Particularely, this system begets the [Madison Park Vocational Machine](https://gitlab.cba.mit.edu/jakeread/mpvm)

![mill](images/mpvm-v03.jpg)

## Building Axis and Machines from 'Kinematic Elements'

Machines are made of degrees of freedom, and those degrees can be assembled from 'Kinematic Elements' i.e. here I have one Motor / Pulley Unit, as well as Lateral (into the gantry) and Cross (across the gantry) supports. I'm doing a mediocre job of explaining this, but here are some images:

To scratch a machine together, I pull elements into Rhino as .step files, and lay them out into axis - here's one linear degree of freedom:

![basically](images/kunits-basic.png)

Then I assemble those axis together, keeping track of where I'll be adding plates of material:

![units xz](images/kunits-before-form.png)

Then I go about filling in detail design, adding tabs etc to bring beams and chassis together.

![one](images/kunits-one.png)

![two](images/kunits-two.png)

![three](images/kunits-three.png)

![four](images/kunits-four.png)

## Fabricating Axis 

I then mill these axis on our shopbot, or any CNC mill you like - the N17 size elements can likely be laser-cut with acrylic or similar (delrin would be nice, but is expensive and a bit toxic to cut, so goes the lore). 

![cam](images/mpvm-rhino-cam.png)

I do CAM in Fusion with a .step I export (after flat-packing) from Rhino.

![mill](images/mill.jpg)

## Assembling Axis

I typically assemble machines one axis at a time. That's not saying much, but here's an image to get a sense of how the hardware goes together:

![one](images/rct-one-rob.jpg)

The 'adjustable' elements use a captured nut to pre-load bearings against gantries. TODO is better documentation of this, but here's an image of the flexure in simulation to get a sense for what I mean:

![sim](images/preload-sim.png)

# CAD for You

### RCT Elements NEMA17 Size 

CAD For these elements is available in the repo [cad/elements/rctElementsN17](cad/elements/rctElementsN17)

This set assumes you're building with 1/4" HDPE or similar, Nema 17 Motors with GT2 Pulleys of 6mm width, and have access to a set of M3 Socket Head Hardware and 'MR115' or '5116' trade names or similar, 5x11x4mm. Lots of those.

### RCT Elements for NEMA23 Size

CAD For these elements is available in the repo [cad/elements/rctElementsN23](cad/elements/rctElementsN23)

This set assumes you're building with 3/8" HDPE or similar, Nema 23 Motors with GT2 Pulleys of 9mm width, and have access to a set of M5 Socket Head Hardware and 'skate bearings' - 608ZZ or similar, 8x22x7mm. Lots of those.

# Machines using RCT Gantries (v1)

![mill](images/mpvm-v03.jpg)
[Madison Park Vocational Machine](https://gitlab.cba.mit.edu/jakeread/mpvmachine)

![smallgantry](images/smallgantries-fab.jpg)
[Small Gantries Workshop Machine](https://gitlab.cba.mit.edu/jakeread/smallgantries)

![claystack](images/claystack.jpg)
[Claystack Ceramics Printer](https://gitlab.cba.mit.edu/jakeread/claystack)