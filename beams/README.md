# Beams for Machines

Torsion ain't easy, as they say. In a break from a bonafide software extravaganza, I've been walking through this design for a decent beam from flat stock, to back on RCT Gantry types.

![beams](images/acrylicbeam.jpg)

The beam design is straightforward, but I've also been occupied with joinery. A big driver here is the desire to make machines with laser cutters and acrylic, to really lower the threshold for fab-your-own-machine-ing. To that end, joints in acrylic that don't cause cracking has been the order.

Typically, laser cut acrylic members are joined a-la this classic machinescrew-and-captured-nut joint:

~ pic of makerbot type joint ~

These are overdue to crack at the stress concentration around the nut:

~ microscope pic of your nut beam ~

This is largely the fault of Acrylic, which, despite its low cost, relative high stiffness (~ 3GPa Modulus), and ease-of-lasercutting (about 50W machine can chew through 7mm OK), is very brittle.

To handle this, I was interested in developing some 3D Printed load-distributing members. I went through a few designs, settling on something like this:

~ pic of joint ~

There are 'singlet', 'tee' and 'corner' members, each having a heat-set insert in the rear, and some arms to grip the reciprocal laser-cut feature. I'm a minor fan of these so far, but I think the design can still be improved. In general, I'd love more algorithmic generation - a mechanically sound method for generating 3D Printable 'node' joinery based on pin-frame beams is a bigger desire.

Beams made up of these joints feel pretty satisfying, although the subtle 'creak' of the acrylic tells me they're not 100% quite yet. However, serviceable.

## Usage

**Material choice**

Like I mentioned, these are sort of designed with Acrylic in mind. That said, moving to Delrin (Acetal) will up the overall durability for a similar modulus, but also double your cost.

Moving to aluminum will similarely double your cost, and put you in a new fab-bracket (I use a waterjet), but brings your beam from 3GPa material -> 60GPa (also ~ 2.5x density). With aluminum, this stress concentration issue isn't so controlling, and the 3D Printed joint effectively adds a relatively-spongey moment in the structural system, so, with complete time, different joinery should be developed for metals. Alas, it'll do as is.

**Be careful about**

- thicknesses: the 3D Printed joints are *much* happier going together if the arms are dialed in to pinch on to laser cut members, so if you're going to assemble lots, test a pair first
- beams on pulley'd axis: there's not room (at the moment) to accomodate for the pulley's return path. when I spin these through CAD->CAM, I add that accomodation manually (in Rhino). this is much easier with wider beams, as is planned for the [little rascal](https://gitlab.cba.mit.edu/jakeread/littlerascal) machine.

**Doing it**

Apart from that, download the .f3d model (should be in this repo, when the update comes), set your parameters, and las / print.

My workflow is like this:

 - make parameters for material thicknesses in beam, and gantry model
 - export both .step
 - (if a pulley) bring both into rhino, modify ears to suit, and add pulley allowance to beam webs
 - las / wj / 3dp

## Todo

### Completion

when acrylic arrives, laser and test assemble, photograph, document.
there's a temptation to update the carriage(s) so that beam-face machine screws can rest non-flush on the surface. this makes assembly much easier
for littleRascal, complete the bupdate:
 - 110mm wide beam, having belt accommodations
 - beam ear accommodations: motor and idler
 - beam support, and support accommodations
 - the same for cable accommodations

### Documentation

nothing is here yet - add
 - f3d and step for beams
 - f3d and step for bmhws (beamhardwares)
