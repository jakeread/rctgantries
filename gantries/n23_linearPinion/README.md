## Gantry for NEMA23 Motor, 0.25" Stock, Pinion-Type Belt

**STATUS:** Works, be careful with Aluminum Flexural elements: size them proplerly. Machines subject to big loades see these deform through their plastic regime, leaving axis loose.

**USED IN:** [Mother Mother](http://gitlab.cba.mit.edu/jakeread/mothermother)

![N23](RCTN23-alu-img.jpg)

Big(ish) Gantries. Uses 3 bearings per corner, with one set riding on flexures for smooth preloaded motion. Cut aluminum or steel rail on WJ with high cut quality, or cut acrylic on laser. 0.25" or similar (6.35mm) stock assumed.

### Development Notes ... 
- meshed belts redux ?
- to rethink flexures, try captured spring steel sheets? do load calcs, comparing to linear rail, for stiffness and permissible linear loads, draw parameter space charts 
- new flexures: no adjustment, just known preload 

### N23 / 0.25" ALU Tools 

A list of the (perhaps) specialty tools you'll want to have to make this gantry: 

Tool | Where Used | McMaster PN or Link
--- | --- | ---
M3 Tap | Extensively | -
M5 Tap | Extensively | -
M6 Tap | Shoulder Bolts | -
Countersink Bit | Flush Mounting | - 

#### BOM N23 0375HDPE

Type | Size | QTY | Where Used | McMaster PN
--- | --- | --- | --- | --- 
Button Head Thread-Forming | No. 6, 3/4" | 10 + (4 * rail tab) (lots) | Connecting Lap and Tab HDPE, Belt Blocks, Chassis | 99512A265
Button Head Thread-Forming | No. 6, 1/2" | 2 | Belt Blocks | 99512A259
Flat Head Thread-Forming | No. 6, 3/4" | 8 | Flush Mounting HDPE | 95893A258
SHCS | M3x40 | 2 | Used *only* when pre-loading bearing rollers | 91292A024
SHCS | M3x30 | 2 | Used *only* when pre-loading bearing rollers | 91292A022
Belleville Washer | 3.1mm ID | 24 | Used *only* when pre-loading bearing rollers | 96445K157
Locknut | M3, Nylon | 6 | Used *only* when pre-loading bearing rollers | 90576A102
Locknut | M5, Nylon | 4 | Connecting Nema 23 Motor | 93625A200
SHCS | M5x10 | 1 | Connecting Nema 23 Motor | 91292A124
SHCS | M5x16 | 3 | Nema 23 Motor through tensioning arcs | 91292A126
Shoulder Screw | 8mm Shoulder x 8mm x M6 | 10 | Guide Roller Shaft | 92981A198
Shoulder Screw | 8mm Shoulder x 16mm x M6 | 2 | Belt Guide Roller Shaft | 92981A202
Bearing Shim | 8mm ID x 10mm OD x 1mm Thick | 38 | Roller Separation | 98089A381 
Less Expensive Bearing Shim | 5/16" Screw Oversize Washer | 38 | Roller Separation | 91090A110


#### BOM N23 025ALU

Type | Size | QTY | Where Used | McMaster PN
--- | --- | --- | --- | --- 
SHCS | M3x12 | 8 | Connecting Lap and Tab Aluminum, and Belt Blocks |  
FHCS | M3x12 | 8 | Nice option for flush-mounting things | 
SHCS | M3x35 | 2 | Used *only* when pre-loading bearing rollers | 91292A033
SHCS | M3x45 | 2 | Used *only* when pre-loading bearing rollers | 91292A025
Belleville Washer | 3.1mm ID | 12 | Used *only* when pre-loading bearing rollers | 96445K157
Locknut | M3, Nylon | 2 | Used *only* when pre-loading bearing rollers | 90576A102
Locknut | M5, Nylon | 4 | Connecting Nema 23 Motor | 93625A200
SHCS | M5x10 | 1 | Connecting Nema 23 Motor |  
SHCS | M5x16 | 3 | Nema 23 Motor through arcs | 
Shoulder Screw | 8mm Shoulder x 8mm x M6 | 10 | Guide Roller Shaft | 92981A198
Shoulder Screw | 8mm Shoulder x 16mm x M6 | 2 | Belt Guide Roller Shaft | 92981A202

### N23 / 0.25" ALU Parts

What | Spec | QTY | Where Used | Link
--- | --- | --- | --- | --- 
608ZZ Bearings | 8x22x7 | 14 | Rollers | [VXB 10](https://www.vxb.com/608ZZ-Shielded-8x22x7-Miniature-Bearing-Pack-of-10-p/608zz10.htm), [VXB 1000](https://www.vxb.com/Wholesale-Lot-of-1000-608ZZ-Ball-Bearing-p/608zz-wholesale.htm)
GT2 Belt | 10mm Wide, Length Dependent | 2 | Belt! | [Amazon](https://www.amazon.com/Timing-Pulley-Teeth-6-35mm-Printer/dp/B07BS4DVR5/)
GT2 Pulley | 10mm Wide, Motor Bore Diameter | 1 | Transmission! | [Above, Combo](https://www.amazon.com/Timing-Pulley-Teeth-6-35mm-Printer/dp/B07BS4DVR5/)

Of course, you'll also need material. In this case, just 0.25" aluminum. I use 6061. 


## Shared Purchase Parts

What | Spec | QTY | Where Used | Link
--- | --- | --- | --- | --- 
608ZZ Bearings | 8x22x7 | 14 | Rollers | [VXB 10](https://www.vxb.com/608ZZ-Shielded-8x22x7-Miniature-Bearing-Pack-of-10-p/608zz10.htm), [VXB 1000](https://www.vxb.com/Wholesale-Lot-of-1000-608ZZ-Ball-Bearing-p/608zz-wholesale.htm)
GT2 Belt | 10mm Wide, Length Dependent | 2 | Belt! | [Amazon](https://www.amazon.com/Timing-Pulley-Teeth-6-35mm-Printer/dp/B07BS4DVR5/)
GT2 Pulley | 10mm Wide, Motor Bore Diameter | 1 | Transmission! | [Above, Combo](https://www.amazon.com/Timing-Pulley-Teeth-6-35mm-Printer/dp/B07BS4DVR5/)
Stepper Motor | NEMA23 x52mm | 1 | Torque ! | [StepperOnline](https://www.omc-stepperonline.com/hybrid-stepper-motor/nema-23-bipolar-18deg-09-nm-1275ozin-2a-36v-57x57x52mm-4-wires-23hs20-2004s.html)
Stepper Motor | NEMA23 x76mm | -or | Torque ! | [StepperOnline](https://www.omc-stepperonline.com/hybrid-stepper-motor/nema-23-bipolar-18deg-19nm-269ozin-28a-32v-57x57x76mm-4-wires-23hs30-2804s.html)