## Gantry for NEMA17 Motor, 0.25" Stock, Pinion-Type Belt

**STATUS:** up to date, works well, feels good, not measured for stiffness or straightness etc.

**USED IN:** [Little Rascal](http://gitlab.cba.mit.edu/jakeread/littlerascal)

![parts](n17_linearPinion_partsCallout.jpg)

![explain](n17_linearPinion_explain.jpg)

![n17sofar](n17-assem.jpg)

Smaller Gantries. Uses 45* bearings in 3DP blocks, with one set riding on flexures for smooth preloaded motion. Cut aluminum or steel rail on WJ with high cut quality, or cut acrylic on laser. 0.25" or similar (6.35mm) stock assumed.

**shoulder bolts can be substituted for fabricated bushings** 

s/o to Rahul in Kerala for this example: 

![bushings](shoulder-alternative.jpg)

### BOM for N17 

Type | Size | QTY | Where Used | Vendor | PN
--- | --- | --- | --- | --- | --- 
Flat Head Thread-Forming | No. 6, 1/2" | 20 | Most Plastic Fasteners | McMaster | 95893A255
Bearing Shims | 5x10x0.5mm | 22 | Adjacent Every Bearing | McMaster | 98089A331
Shoulder Bolts | 5x10xM4 | 2 | Pulleys | McMaster | 92981A030 
Shoulder Bolts | 5x6xM4 | 8 | Rollers | McMaster | 92981A146
Heat-Set M4 Tapered Inserts | M4 | 10 | Anchors for Sholder Bolts into Plastic Parts | McMaster | 94180A351
Heat-Set M3 Tapered Insert | M3 | 3 | Threads for M3 Screws to Adjust Motor Tension and Bearing Preload | McMaster | 94180A331
M3 SHCS | 10mm Long | 4 | Motor Mounting | McMaster | 91292A113
M3 SHCS | 14mm Long | 2 | Bearing Preloading | McMaster | 91292A027
M3 SHCS | 20mm Long | 1 | Tensioning Pulley | McMaster | 91292A027
M3 Washers | - | 7 | Screws without Washers are Sad Screws | McMaster | 93475A210
M4 SHCS | 20mm Long | 4 | Belt Mounting | McMaster | 91292A121
M4 Washers | - | 4 | ibid | McMaster | 93475A230 
625ZZ Bearings | 5x16x5mm | 12 | - | VXB | [link](https://www.vxb.com/20-625ZZ-Shielded-5mm-Bore-Diameter-Miniature-p/625zz20.htm) or McMaster 6153K113
20T or 16T GT2 Pulley | 10mm or Wider, 5mm Bore | 1 | - | Amazon or SDP/SI | [Amazon Link](https://www.amazon.com/uxcell-Aluminum-Timing-Synchronous-Printer/dp/B072M2CM1W/)
10mm (or 9mm) Wide GT2 Belt | Find Steel-Core for Stiffness! | 1 | - | Amazon or SDP/SI | [Amazon Link](https://www.amazon.com/BALITENSEN-Black-Timing-Printer-Machine/dp/B07F2X236H/)	

### Materials for N17

1/4" Acrylic from McMaster, for 12x24" for small lasers pick 8505K755   
1/4" ALU 6061 Typical (6.35mm)