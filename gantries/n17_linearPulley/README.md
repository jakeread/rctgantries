## Gantry for NEMA17 Motor, 0.25" Stock, Pulley-Type Belt

**STATUS:** mistakes abound, works but assembly and belt-tensioning are not wonderful. CAD model and actual typical assembly do not reflect one another.

**USED IN:** [Little Rascal](http://gitlab.cba.mit.edu/jakeread/littlerascal)

![n17sofar](n17pulley.jpg)

Smaller Gantries. Uses 45* bearings in 3DP blocks, with one set riding on flexures for smooth preloaded motion. Cut aluminum or steel rail on WJ with high cut quality, or cut acrylic on laser. 0.25" or similar (6.35mm) stock assumed.

![n17sofar](images/n17-with-n23.jpg)

The dowel pin you're looking for: https://www.mcmaster.com/91585A559

### Development Notes ...
- motor mount should have flush-type inset like pinion motor mount
 - motor mount also should include more accomodation to take up stress with highly taught belts, deflection can be seen atm
 - 3d printing this piece blows, doesn't have to ?
 - remove those tracking threads, they're unusable
 - move motor tensioning thread so that carriage does not find it

- upside-down pulley mount ... and tack-on belt clamp, need formalization and improvement. to keep the minimum width where it is, you can mount 'down the middle' and use the space on the sides for the plastic thread-forming screw

- increase the width on the idler, more shims in between - tracking needs to breath a bit more

- ok so
 - no tension at the motor, just a *juicy* mount
 - this winged pad to clamp the pulley beneath
 - fulstop tension & track w/ press-in idler at the other end,
