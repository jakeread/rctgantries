## Gantry for NEMA17 Motor, 0.25" Stock, Pinion-Type Belt with Reduction

**STATUS:** looks and feels sound by first test, tbd on documentation

![n17lp](n17-hp-01.jpg)

![n17lp](n17-hp-02.jpg)

![n17lp](n17-hp-03.jpg)
