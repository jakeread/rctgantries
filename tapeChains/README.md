## HQ Wire Bundling

Cable Routing is the ad-hoc machine builder's 'come to jesus' moment, every time. Here is a project that aims to help ye faithful straighten out.

Original credit to [the unlinked hero in this Hackaday article](https://hackaday.com/2017/12/17/diy-cable-chain-looks-great-stays-cheap/)

Trying this with Heat Shrinks
 - -> 1" 3M161381-ND
 - -> 0.75" 3M161354-ND 
 - -> 0.5" A100C-4-ND
 - -> 0.375" A034C-4-ND 

 - -> 3/8" ID Sleeve 2837K13
 - -> 1/2" 2837K32
 - -> 3/4" 2837K33
 - -> 1"  2837K34

 - Stanley Model 33-116, 6802A36 (3/4" width)
 - Stanley Model 33-716, 6817A1 (1 1/4" width) 

![tapeChains](tapeChains-1.jpg)

![tapeChains](tapeChains-2.jpg)