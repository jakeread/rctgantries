# Roller Coaster Gantries

These are machine design elements that we use at the CBA when we're spinning up custom machines: they're parametric, aim to be reconfigurable, and we can make them in the lab with a minimum set of stock hardware.

## Machines Using RCT Gantries

[Little Rascal](https://gitlab.cba.mit.edu/jakeread/littlerascal)
[Mother Mother, a generalist](https://gitlab.cba.mit.edu/jakeread/mothermother)
[Claystack, a ceramics 3d printer](https://gitlab.cba.mit.edu/jakeread/claystack)
[the Madison Park Vocational Machine, for high schools](https://gitlab.cba.mit.edu/jakeread/mpvmachine)

# Usage

This repository should help you figure out how to build your own axis: it contains some explanatory drawings, videos, as well as CAD files and Bills of Materials for various design elements.

A typical machine-level workflow is described [for machineweek, at this link](https://gitlab.cba.mit.edu/jakeread/machineweek-2018).

![](video/SEQ-fusion-parametric-enc.mp4)

First, find the model you'd like to start with on this page and download it, or clone the whole repo. The ```.f3d``` files are parametric Fusion 360 files. Files here are tuned for material thickness choices - but the exact thickness of your material can be tuned in the model: i.e. the 0.375" HDPE model will also work for ~ 8 - 11mm HDPE if you're working with sensible units in another country.

In Fusion, you can open this file up and use (from the top menu)

``` Modify >> Change Paremeters ```

Each of these models should have some parameters starred, these are what you'll want to configure. Go ahead and set axis lengths and material thicknesses according to what you're doing.

There are also two sets of hole patterns in the last two 'groups' of features on the feature history bar. One puts a 20mm square grid on top of the axis, the other on the bottom. You can leave these in, or customize them, or add whatever mounting-to-the-next-bit design you'd like.

When you're satisfied, you can export the model as a .step file, using the file menu, to prep it for fabrication.

``` File >> Export ```

Make sure to change 'type' to .step, and check the 'save to my computer' box. Now you're ready to import it into another assembly.

**Alternately,** you can save the changes to a new file and import that model into another Fusion 360 Assembly.

# Material Choice

I've made a little table of different materials we can use. Typically, I stick with Aluminium (on a waterjet) or Acrylic (on a laser). Alternately, Delrin or HDPE can be machined, Delrin is medium-ok to lasercut, and I haven't tried (but am curious about) composites like FR4 and FR1. Probably more towards FR1: glass is a PITA to machine.

In the fullness of time, and with infinite fab, I figure gantry components (especially elements which see bearing contact) should be made from steels, water or laser-cut.

| Material | Young's Modulus (GPA) | Specific Young's | Cost for 6mm x 24x24" | Machinability |
| --- | ---: | ---: | --- | --- |
| ABS | 2 | ? | 52 | Not Dimensionally Stable, but OK to Machine |
| Nylon 6 | 3 | 2.5 | 130 | Painful |
| HDPE | 1 | ? | 23 | Easy |
| Acetal (Delrin) | 2.8 | ? | 89 | Breezy, also lasers, and non-cracking |
| Cast Acrylic | 3.3 | 2.8 | 46 | Breezy, esp. w/ Lasers |
| 6061 ALU | 69 | 22 | 87 | Breezy with WJ, Painful on Shopbot |
| FR1/CE (Canvas / Phenolic) | 6 | ? | 81 | TBD, probably WJ Pain and Ease on SB |
| FR4/G10 (Fiberglass) | 22 | ? | 98 | Painful on a WJ, Slightly Easier on a Shopbot |

## The Blocks

### [N17 Linear and Pinion-Type Belt](gantries/n17_linearPinion/)
![n17lp](gantries/n17_linearPinion/n17-assem.jpg)

### [N17 Linear and Pinion-Type Belt, Reduced](gantries/n17_heavyPinion/)
![n17lp](gantries/n17_heavyPinion/n17-hp-01.jpg)

### [N17 Linear and Pulley-Type Belt](gantries/n17_linearPulley/)
![n17pp](gantries/n17_linearPulley/n17pulley.jpg)

### [N23 Linear and Pinion-Type Belt](gantries/n23_linearPinion/)
![n23lp](gantries/n23_linearPinion/RCTN23-alu-img.jpg)

## The Beams

### [Torsion Ain't Easy](beams/)
![beams](beams/images/acrylicbeam.jpg)

## Bonus Kit

### [E Chain with Tape Measure Backbone](tapeChains/)

![tc](tapeChains/tapeChains-2.jpg)
